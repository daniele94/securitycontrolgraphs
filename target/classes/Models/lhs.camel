camel model LsyModel {
	
	application LsyPrototype {
		version: "v1.0"
		owner: LsyOrganization.StefanSpahr
		deployment models [LsyModel.LsyDeployment]
	}
	
	organisation model LsyOrganization {//
		organisation LSY {
			www: "http://www.lhsystems.com"
			postal address: "Am Prime Parc 9 D-65479 Raunheim"
			email: "john.doe@acme.com"
		}
		
		user StefanSpahr {
			first name: John
			last name: Doe
			email: 'John.Doe@acme.com'
			
			// TODO MUSA: change paasage credentials to musa credentials
			musa credentials {
			username: 'user'
			password:  'pass' // the credentials to authenticate a user in the MUSA platform
            }            // example made by TEC. 
            /* commented by TECNALIA
            // The cloud provider GWDG is defined in another script for the sake of clarity
			cloud credentials [
				// to authenticate and authorise the user in a cloud provider and hence enable performing
				// tasks on behalf of the user on the cloud provide
				id1 { username:  'user2' password: 'pass2' cloud provider: GWDGModel.GWDGOrganisation.GWDG }
			] // cloud credentials
			*/
		} // user StefanSpahr
		
		security level: HIGH
	} // organisation model LsyOrganization
	
	
	unit model LsyUnit {
		monetary unit {
			Euro: EUROS
		}
		
		dimensionless {
			CPUUnit: PERCENTAGE
		}
		
		time interval unit {
			ResponseTimeUnit: MILLISECONDS
		}
		
		dimensionless {
			AvailabilityUnit: PERCENTAGE
		}
 
        /* changed by TEC 
		transaction unit  {
			MessagesCount: TRANSACTIONS/SECOND 
		}
		* 
		*/
        throughput unit {
        	MessagesCount: TRANSACTIONS_PER_SECOND
        }		
	}
	
	location model LsyLocations {
		region Europe {
			name: "Europe"
		}
		
		country Germany {
			name: "Germany"
			parent regions [LsyLocations.Europe]
		}
		
		country UK {
			name: "UnitedKingdom"
			parent regions [LsyLocations.Europe]
		}
		
		region NorthAmerica {
			name: "North America"
		}
		
		country USA {
			name: "USA"
			parent regions [LsyLocations.NorthAmerica]
		}
		
		region Asia {
			name: "Asia"
		}
		
		country Singapur {
			name: "Singapur"
			parent regions [LsyLocations.Asia]
		}
	}

	
	deployment model LsyDeployment {		
		// requirements about metrics required for composition???
		
		//Requirement sets
		requirement set CPUIntensiveUbuntuReqSet {
			os: LsyRequirements.Ubuntu64
			quantitative hardware: LsyRequirements.CPUIntensive 
		}
		
		requirement set MemoryIntensiveUbuntuReqSet {
			os: LsyRequirements.Ubuntu64
			quantitative hardware: LsyRequirements.MemoryIntensive
		}
		
		requirement set StorageIntensiveUbuntuReqSet {
			os: LsyRequirements.Ubuntu64
			quantitative hardware: LsyRequirements.StorageIntensive
		}
		
		requirement set BasicUbuntuReqSet {
			os: LsyRequirements.Ubuntu64
			quantitative hardware: LsyRequirements.Basic
		}
		
		//Virtual machines
		vm CPUIntensiveUbuntu {
			requirement set CPUIntensiveUbuntuReqSet
			provided host CPUIntensiveUbuntuHost
		}
		
		vm MemoryIntensiveUbuntu {
			requirement set MemoryIntensiveUbuntuReqSet
			provided host MemoryIntensiveUbuntuHost
		}
		
		vm StorageIntensiveUbuntu {
			requirement set StorageIntensiveUbuntuReqSet
			provided host StorageIntensiveUbuntuHost
		}
		
		vm BasicUbuntu {
			requirement set BasicUbuntuReqSet
			provided host BasicUbuntuHost
		}
		
		//Components
		internal component FleetWriteModel {		
			provided communication FleetWriteModelPort{port: 8080 }
			
			required communication ZookeeperPortReq{port: 2181 mandatory }
			required communication KafkaPortReq{port: 9092 mandatory }
			required communication MongoDbPortReq{port: 27017 mandatory }
			
			
			required host CPUIntensiveUbuntuReqSet
			
		/**************
		 ** One of two possible configurations will be used. 
		 *  How have we to mark it???
		 **************/			
			
			/*
			 * MUSA repos to download the artifacts of LHS
			 
			configuration FleetWriteModelScript {
				download: 'curl --user balazs.somoskoi:Qwe12345 -L -O http://150.241.54.140:7990/projects/MUSA/repos/musa-nlsched/browse/deployables/2016.02.29-fleet-manager-write-model-app.jar'
				install: ''
				start: 'java -jar 2016.02.29-fleet-manager-write-model-app.jar'
				
			}*/


		configuration FleetWriteModelConfiguration{
			CHEF configuration manager ChefFleetWriteModelConfiguration { // one of the configuration management tools pre-defined
				cookbook: 'Containers'
				recipe: 'nlsched-fleet-write'
			}
		}
	
		}
		
		internal component FleetReadModel {			
			provided communication FleetReadModelPort{port: 8080 }
			
			required communication ZookeeperPortReq{port: 2181 mandatory }
			required communication KafkaPortReq{port: 9092 mandatory }
			required communication MongoDbPortReq{port: 27017 mandatory }
			

			required host CPUIntensiveUbuntuReqSet
			
			/*
			configuration FleetReadModelScript {
				download: 'curl --user balazs.somoskoi:Qwe12345 -L -O http://150.241.54.140:7990/projects/MUSA/repos/musa-nlsched/browse/deployables/2016.02.29-fleet-manager-read-app.jar'
				install: ''
				start: 'java -jar 2016.02.29-fleet-manager-read-app.jar'
			}*/	
			
		    configuration FleetReadModelConfiguration{
				CHEF configuration manager  ChefFleetReadModelConfiguration { // one of the configuration management tools pre-defined
					cookbook: 'Containers'
					recipe: 'nlsched-fleet-read'
				}
			}
		}
		
		internal component Kafka {		
			provided communication KafkaPort{port: 9092 }
			
			required communication ZookeeperPortReq{port: 2181 mandatory }
			
			required host StorageIntensiveUbuntuReqSet
		    configuration KafkaConfiguration{
				CHEF configuration manager ChefKafkaConfiguration{ // one of the configuration management tools pre-defined
					cookbook: 'Containers'
					recipe: 'kafka'
				}			
			}
		}
		
		internal component Zookeeper {
			provided communication ZookeeperPort{port: 2181 }
			
			required host BasicUbuntuReqSet
			
		    configuration ZookeeperConfiguration{
				CHEF configuration manager ChefZookeeperConfiguration { // one of the configuration management tools pre-defined
					cookbook: 'Containers'
					recipe: 'zookeeper'
				}			
			}
		}
		
		/** it requires HAProxy - http://www.haproxy.org/ 
		 * Not HA Framework
		 */
		internal component CentralGateway {
			provided communication CentralGatewayPort{port: 80 }
			
			required communication SecurityPortReq{port: 8080 mandatory }
			required communication FleetReadModelPortReq{port: 8080 mandatory }
			required communication FleetWriteModelPortReq{port: 8080 mandatory }
			
			required host CPUIntensiveUbuntuReqSet
			
		    configuration CentralGatewayConfiguration{
				CHEF configuration manager ChefCentralGatewayConfiguration { // one of the configuration management tools pre-defined
					cookbook: 'Containers'
					recipe: 'centralgateway'
				}			
			}
		}
		
		/** similar to Docker concept 
		internal component  HAProxy {
			
		}
		* */


		
		internal component Security {
			provided communication SecurityPort{port: 8080 }
			
			required communication KafkaPortReq{port: 9092 mandatory }
			required communication ZookeeperPortReq{port: 2181 mandatory }
			required communication MongoDbPortReq{port: 27017 mandatory }
			
			required host CPUIntensiveUbuntuReqSet
			
			/*configuration SecurityScript {
				download: 'curl --user balazs.somoskoi:Qwe12345 -L -O http://150.241.54.140:7990/projects/MUSA/repos/musa-nlsched/browse/deployables/2016.02.29-security-service.jar'
				install: ''
				start: 'java -jar 2016.02.29-security-service.jar'
			}*/
		    configuration SecurityConfiguration{
				CHEF configuration manager  ChefSecurityConfiguration { // one of the configuration management tools pre-defined
					cookbook: 'Containers'
					recipe: 'security'
				}			
			}			
		}
		
		internal component MongoDb {
			provided communication MongoDbPort{port: 27017 }
			
			required host StorageIntensiveUbuntuReqSet
			
		    configuration MongoDbConfiguration{
				CHEF configuration manager ChefMongoDbConfiguration { // one of the configuration management tools pre-defined
					cookbook: 'Containers'
					recipe: 'mongo'
				}			
			}
		}
		
		
		//hosting
		hosting FleetWriteModelHosting {
			from FleetWriteModel.CPUIntensiveUbuntuReqSet to CPUIntensiveUbuntu.CPUIntensiveUbuntuHost
		}
		
		hosting FleetReadModelHosting {
			from FleetReadModel.CPUIntensiveUbuntuReqSet to CPUIntensiveUbuntu.CPUIntensiveUbuntuHost
		}
		
		hosting KafkaHosting {
			// changed  from MemoryIntensiveUbuntuReqSet to StorageIntensiveUbuntuReqSet by TEC
			from Kafka.StorageIntensiveUbuntuReqSet to MemoryIntensiveUbuntu.MemoryIntensiveUbuntuHost
		}
		
		hosting ZookeeperHosting {
			// changed from MemoryIntensiveUbuntuReqSet to BasicUbuntuReqSet by TEC
			from Zookeeper.BasicUbuntuReqSet to MemoryIntensiveUbuntu.MemoryIntensiveUbuntuHost
		}
		
		hosting CentralGatewayHosting {
			from CentralGateway.CPUIntensiveUbuntuReqSet to CPUIntensiveUbuntu.CPUIntensiveUbuntuHost
		}
		
		hosting SecurityHosting {
			from Security.CPUIntensiveUbuntuReqSet to CPUIntensiveUbuntu.CPUIntensiveUbuntuHost
		}
		
		hosting MongoDbHosting {
			from MongoDb.StorageIntensiveUbuntuReqSet to StorageIntensiveUbuntu.StorageIntensiveUbuntuHost
		}
		
		//communication //--Type is not specified right now--
		communication KafkaToZookeeper {
			type: ANY // example made by TEC
			from Kafka.ZookeeperPortReq to Zookeeper.ZookeeperPort
		}
		
		communication CentralGatewayToSecurity {
			type: LOCAL // example made by TEC
			from CentralGateway.SecurityPortReq to Security.SecurityPort
		}
		
		communication CentralGatewayToFleetReadModel {
			type: REMOTE // example made by TEC
			from CentralGateway.FleetReadModelPortReq to FleetReadModel.FleetReadModelPort
		}
		
		communication CentralGatewayToFleetWriteModel {
			type: ANY // example made by TEC
			from CentralGateway.FleetWriteModelPortReq to FleetWriteModel.FleetWriteModelPort
		}
		
		communication SecurityToKafka {
			type: ANY // example made by TEC
			from Security.KafkaPortReq to Kafka.KafkaPort
		}
		
		communication SecurityToZookeeper {
			type: REMOTE // example made by TEC
			from Security.ZookeeperPortReq to Zookeeper.ZookeeperPort
		}
		
		communication SecurityToMongoDb {
			type: ANY // example made by TEC
			from Security.MongoDbPortReq to MongoDb.MongoDbPort
		}
		
		communication FleetReadModelToKafka {
			type: ANY // example made by TEC
			from FleetReadModel.KafkaPortReq to Kafka.KafkaPort
		}
		
		communication FleetReadModelToZookeeper {
			type: ANY // example made by TEC
			from FleetReadModel.ZookeeperPortReq to Zookeeper.ZookeeperPort
		}
		
		communication FleetReadModelToMongoDb {
			type: ANY // example made by TEC
			from FleetReadModel.MongoDbPortReq to MongoDb.MongoDbPort
		}
		
		communication FleetWriteModelToKafka {
			type: ANY // example made by TEC
			from FleetWriteModel.KafkaPortReq to Kafka.KafkaPort
		}
		
		communication FleetWriteModelToZookeeper {
			type: ANY // example made by TEC
			from FleetWriteModel.ZookeeperPortReq to Zookeeper.ZookeeperPort
		}
		
		communication FleetWriteModelToMongoDb {
			type: ANY // example made by TEC
			from FleetWriteModel.MongoDbPortReq to MongoDb.MongoDbPort
		}		
	}
	
	requirement model LsyRequirements {	
		//Locations
		location requirement GermanyLocationReq {
			locations [LsyLocations.Germany]
		}
		
		location requirement EuropeLocationReq {
			locations [LsyLocations.Europe]
		}
		
		location requirement AsiaLocationReq {
			locations [LsyLocations.Asia]
		}
		
		location requirement NorthAmericaLocationReq {
			locations [LsyLocations.NorthAmerica]
		}
		
		//Scales
		horizontal scale requirement HorizontalScaleFleetWriteModel {
			component: LsyModel.LsyDeployment.FleetWriteModel
			instances: 1..2
		}
		
		horizontal scale requirement HorizontalScaleFleetReadModel {
			component: LsyModel.LsyDeployment.FleetReadModel
			instances: 2..5
		}
		
		//OS
	    os Ubuntu64 {os: 'Ubuntu' 64os}
		
		//Hardware
		quantitative hardware CPUIntensive {
			core: 1..       // min and max number of CPU cores
			ram: 4096..8192 // size of RAM 
			cpu: 1.0..      // min and max CPU frequency
		}
		
		quantitative hardware MemoryIntensive {
			ram: 8192.. //min size of RAM
		}
		
		quantitative hardware StorageIntensive {
			storage: 1000.. //min storage size
		}
		
		quantitative hardware Basic {
			core: 1..4 // min and max number of CPU cores
			ram: 1024..4096 // size of RAM 
			cpu: 1.0..3.0  // min and max CPU frequency
		}    	
    	
		//--This is not necessary right now--
		//optimisation requirement MaximiseAvvilabilityFleetReadModel {
    	//	function: MAX
    	//	metric: LsyModel.LsyMetrics.AvailabilityMetric
    	//	component: LsyModel.LsyDeployment.FleetReadModel
    	//	priority: 5.0
    	//}
    	
    	/* commented by TECNALIA
    	slo FleetReadModelResponseTimeSLO {
			//--The value  of this time is not specified right now--
    		service level: LsyModel.LsyMetrics.FleetReadModelResponseTimeCondition
    	}
    	*/
	}
	
	type model LsyType {	
		range Range_0_100 {
			primitive type: IntType
			lower limit {int value 0 included}
			upper limit {int value 100}
		}
		
		range Range_0_10000 {
			primitive type: IntType
			lower limit {int value 0}
			upper limit {int value 10000 included}
		}
		
		range DoubleRange_0_100 {
			primitive type: DoubleType
			lower limit {double value 0.0 included}
			upper limit {double value 100.0 included}
		}		
	}


    scalability model LsyScalability {
       // TODO in the second iteration   
    }
    
	metric model LsyMetric {
       // TODO in the second iteration		
	}
}
