package eu.musaproject.multicloud.engines.deployments;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.Map.Entry;

import eu.musaproject.multicloud.models.graphs.Peer;
import eu.musaproject.multicloud.models.macm.MACM;
import eu.musaproject.multicloud.models.macm.macmPeerType;

public abstract class Deployment implements DeploymentInterface{
	Properties prop;
	Component[] Cindex;
	VM[] offering;
	public DeploymentStates status=DeploymentStates.uncreated;
	
	enum DeploymentStates { uncreated, undeployed, deployed} 
	
	public abstract void setApp(Component[] Comps);
	
	public void setApp(MACM macm) {
		Component[] comps=new Component[macm.getPeers().size()];
		int i=0;
		for (Entry<String, Peer> entry: macm.getPeers().entrySet()){
			if (entry.getValue().getType()==macmPeerType.SaaS) {
				comps[i]=new Component(""+i+","+entry.getValue().getName()+",1024,100");
			}
			if (entry.getValue().getType()==macmPeerType.PaaS) {
				comps[i]=new Component(""+i+","+entry.getValue().getName()+",1024,100");
			}
			i++;
		}
		setApp(comps);
	}
	
	public abstract void fromVector(int[] depl,int[]concrete);
	
	public VM[] getOffering() {
		return offering;
	}
	
	public Component[] getComponents() {
		return Cindex;
	}
	
	public void readComponents(String Filename) {
		prop = new Properties();
		try {
			prop.load(new FileInputStream(Filename));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String[] comps= (prop.getProperty("Comps")).split(";");
		Cindex= new Component[comps.length];
		for (int i=0;i<comps.length;i++) {
			Cindex[i]= new Component(comps[i].replaceAll("\\s+",""));
		}
	}
	
	public void readOfferings(String Filename) {
		prop = new Properties();
		try {
			prop.load(new FileInputStream(Filename));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String[] offs= (prop.getProperty("VMs")).split(";");
		offering= new VM[offs.length];
		for (int i=0;i<offs.length;i++) {
			offering[i]= new VM(offs[i].replaceAll("\\s+",""));
		}
	}
	
	public void print() {
		if (status==Deployment.DeploymentStates.deployed) {
			System.out.println("Deployed MACM");
		}
	}
	public void printOffering() throws MissingOfferingException {
		if (offering==null) throw new MissingOfferingException();
		String off="";
		for (int i=0;i<offering.length;i++) off+="-"+i+":"+offering[i];
		System.out.println(off);
	}
}
