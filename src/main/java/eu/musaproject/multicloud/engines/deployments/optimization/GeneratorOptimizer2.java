package eu.musaproject.multicloud.engines.deployments.optimization;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.jenetics.IntegerChromosome;

import eu.musaproject.multicloud.engines.deployments.Component;
import eu.musaproject.multicloud.engines.deployments.Deployer;
import eu.musaproject.multicloud.engines.deployments.Deployment;
import eu.musaproject.multicloud.engines.deployments.DeploymentInterface;
import eu.musaproject.multicloud.engines.deployments.DeploymentMapBased;
import eu.musaproject.multicloud.engines.deployments.MissingComponentsException;
import eu.musaproject.multicloud.engines.deployments.MissingOfferingException;
import eu.musaproject.multicloud.engines.deployments.VM;
import eu.musaproject.multicloud.engines.deployments.generators.multiCSPDeployer;
import eu.musaproject.multicloud.models.graphs.Peer;
import eu.musaproject.multicloud.models.macm.MACM;
import eu.musaproject.multicloud.models.macm.macmPeerType;
import eu.musaproject.multicloud.utilities.NeoDriver;

public class GeneratorOptimizer2 extends Deployer {
	multiCSPDeployer mcgen;
	int nComponents;
	int nCSP;
	VM[] offering;
	Component[] Cindex;
	int [] logical;
	int [] concrete;
	double cost;
	String optData="";
	String solution="";
	boolean log=false;


	public GeneratorOptimizer2() {
		super();
		mcgen=null;
		validEvaluated=0;
	}
	
	public GeneratorOptimizer2(Deployment dep) {
		setDeployment(dep);
		validEvaluated=0;

	}
	
	public DeploymentInterface setDeployment(DeploymentInterface depl) {
		myDeployment=depl;
		super.myDeployment=myDeployment;
		offering=myDeployment.getOffering();
		nCSP=offering.length;
		Cindex=myDeployment.getComponents();
		nComponents=Cindex.length;
		return myDeployment;
	}
	
	public boolean valid() {
		return enoughResource();
	}
	
	public double fitness() {
		return cost();
	}
	public void optimize(){
		int[] l=mcgen.start();
		long k=0;
		double tmpcost;
		String tmpsol;
		boolean ok;
//		Scanner scan=new Scanner(System.in );
		while(l!=null) {
			k++;
//		    System.out.println(k+": ");
//		    mcgen.print();
		    tmpsol=""+k+":"+mcgen.asString();
		    tmpcost=fitness();
		    tmpsol=""+k+":"+mcgen.asString()+" cost:"+tmpcost;
		    ok=valid();
			if (ok&&(cost>tmpcost)) {
				cost=tmpcost;
				logical=mcgen.getLogicalDeployment().clone();
				for (int i=0;i<logical.length;i++) {
					logical[i]--;
				}
				concrete=mcgen.getConcreteDeployment().clone();					
			}
//			System.out.println("Previous Cost"+cost+ " Actual Cost: "+tmpcost);
			if (ok) tmpsol+=" valid"; else tmpsol+=" invalid";				
			if (log) System.out.println(tmpsol);
			System.out.print("Combinations:"+ k+"\r");

//			System.out.print("Press any key to continue . . . ");
//			scan.nextLine();
			l=mcgen.next();
		}
		System.out.println("");
		evaluated=k;
	}

	
	@Override
	public DeploymentInterface deploy() {
		prepare();
		optimize();
		System.out.println("Compared "+evaluated+" deployments");
//		mcgen.print();
		mcgen.printLexical(logical,concrete);
		System.out.println("Cost:"+cost);
		for (int i=0;i<logical.length;i++) {
			System.out.print(" "+logical[i]);
		}
		System.out.print(" - ");
		for (int i=0;i<concrete.length;i++) {
			System.out.print(" "+concrete[i]);
		}
		System.out.println("cost: "+cost);

		myDeployment.fromVector(logical,concrete);
		return myDeployment;
	}

	private void prepare() {
		List<Peer> components=mymacm.getPeersByType(macmPeerType.SaaS,macmPeerType.PaaS);
		nComponents=components.size();
		offering=myDeployment.getOffering();
		Cindex=myDeployment.getComponents();
		nCSP=offering.length;
		mcgen=new multiCSPDeployer(nComponents,nCSP);	
		System.out.println("nComponents: "+nComponents+" nOfferings:"+nCSP);
		cost=1000000000;
		logical=null;
		concrete=null;
	}


	protected double cost() {
		double c=0;
		int vmindex=-1;
		
		//get the concrete deployment
		int [] concrete=mcgen.getConcreteDeployment();
		for (int i=0; i<concrete.length;i++) {
			vmindex= concrete[i];
			c+=offering[vmindex].cost;
		}
		return c;
	}
	
	public boolean enoughResource() {
		boolean ok=true;
		int [] logical=mcgen.getLogicalDeployment();
		int [] concrete=mcgen.getConcreteDeployment();
		int [] freecpu= new int[concrete.length];
		int [] freemem= new int[concrete.length];
		int vmindex=-1;
		int i;
		//setup the total avilable cpu and mem for each vm
		for (i=0;i<concrete.length;i++) {
			vmindex=concrete[i];
			freecpu[i]=offering[vmindex].CPU;
			freemem[i]=offering[vmindex].RAM;			
		}
		
		//allocate mem and cpu for each component
		for (i=0; i<Cindex.length;i++ ) {
			vmindex=logical[i]-1;
			freecpu[vmindex]-=Cindex[i].CPU;
			freemem[vmindex]-=Cindex[i].RAM;
			if ((freecpu[vmindex]<0)||(freemem[vmindex]<0)) ok=false;
		}
//		for (i=0;i<concrete.length;i++) {
//			System.out.println("VM_"+concrete[i]+" CPU: "+freecpu[i]+"RAM: "+freemem[i]);
//		}
		if (ok) {
//			System.out.println("Valid ");
			validEvaluated++;
		}
//		else System.out.println("Invalid "); 
		return ok;
	}
	
	
	private DeploymentMapBased produceDeployment() {
		// TODO Auto-generated method stub
		myDeployment.fromVector(logical,concrete);
		return null;
	}

	@Override
	public DeploymentInterface refreshDeployment() {
		// TODO Auto-generated method stub
		//create mcgen according to MACM...
		//create components map and ...
		myDeployment.fromVector(logical,concrete);
		return myDeployment;
	}

	public static void main(String args[]) throws MissingOfferingException, MissingComponentsException {
		try {
			NeoDriver.cleanDB();
			NeoDriver.executeFromFile("src/main/resources/optimization/webapp.cyber");
			MACM macm= new MACM();
			macm.readNeo();
			Deployer deployer=new GeneratorOptimizer2();
			deployer.setMACM(macm);
			Scanner scan=new Scanner(System.in);
			System.out.println("Ready to optimize");
			System.out.print("Press any key to continue . . . ");
			scan.nextLine();
			deployer.deploy();	
			deployer.printDeployment();
			MACM m2=deployer.getDeployable();
			NeoDriver.cleanDB();
			m2.writeNeo();
			deployer.printDeployment();
//			System.exit(0);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void printOptimizationData() {
		// TODO Auto-generated method stub
		System.out.println("Optimization Data:");
		System.out.println("Total Configurations:"+evaluated);
		System.out.println("Valid:"+validEvaluated);
		System.out.println("Cost:"+cost);
	}
	
	public String problemDescriptor() {
		String res="";
		res+="Components ("+nComponents+"):";
		for (int i=0;i<nComponents;i++) {
			res+="["+Cindex[i].CPU+","+Cindex[i].RAM+"] ";
		}
		res+="\n";
		res+="Offerings ("+offering.length+"):";
		for (int i=0;i<offering.length;i++) {
			res+="["+offering[i].CPU+","+offering[i].RAM+","+offering[i].cost+"] ";
		}
		res+="\n";
		return res;
	}
	
	@Override
	public String getOptimizationData() {
		String res="";
		res+="Total Configurations:"+evaluated+"\n";
		res+="Valid:"+validEvaluated+"\n";
		res+="Cost:"+cost+"\n";
		return res;
	}

	@Override
	public String getOptimizedSolution() {
		return myDeployment.getAsString();
	}
	
}
