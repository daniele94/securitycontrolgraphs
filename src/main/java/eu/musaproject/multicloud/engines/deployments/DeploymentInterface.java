package eu.musaproject.multicloud.engines.deployments;

import eu.musaproject.multicloud.models.macm.MACM;

public interface DeploymentInterface {
	public VM[] getOffering();
	public Component[] getComponents() ;
	
	public void readOfferings(String offerings);
	public void readComponents(String components);

	
	public void setApp(MACM macm) throws MissingOfferingException,MissingComponentsException;
	public MACM getDeployable(MACM macm);
	public MACM getDeployable();
	
	public double cost();
	public boolean valid();
	
	public void print();
	public void printData();
	public void printDeploymentData();
	public void fromVector(int[] logical, int[] concrete);
	public String getAsString();
	
}
