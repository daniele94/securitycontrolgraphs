package eu.musaproject.multicloud.engines.deployments;

import java.io.IOException;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Scanner;

import eu.musaproject.multicloud.engines.deployments.optimization.GeneratorOptimizer2;
import eu.musaproject.multicloud.engines.deployments.optimization.GeneticOptimizer2;
import eu.musaproject.multicloud.models.graphs.Peer;
import eu.musaproject.multicloud.models.macm.MACM;
import eu.musaproject.multicloud.models.macm.macmPeerType;
import eu.musaproject.multicloud.utilities.NeoDriver;

public abstract class Deployer {
	protected MACM mymacm;
	protected DeploymentInterface myDeployment;
	protected static long evaluated;
	protected static long validEvaluated;
	protected static double finalcost;
	protected String configurationFileName;
	
	protected 	Properties configuration;
	
	public double getFinalcost() {
		return finalcost;
	}

	public  void setFinalcost(double finalcost) {
		Deployer.finalcost = finalcost;
	}

	public  long getEvaluated() {
		return evaluated;
	}

	public  void setEvaluated(int evaluated) {
		Deployer.evaluated = evaluated;
	}

	public abstract DeploymentInterface deploy();
	
	protected Properties defaultConf()  {
		Properties p=new Properties();
		return p;
	}
	
	public Deployer(){
		configuration=defaultConf();
		myDeployment=new LexicalDeployment();
		mymacm=null;
		evaluated=0;
	}

	public Deployer(MACM m) throws MissingOfferingException, MissingComponentsException {
		setMACM(m);
		evaluated=0;
	}
	
	public void setOfferings(String offering) {
		myDeployment.readOfferings(offering);
	}
	
	public void configure() throws WorkInProgressException  {
		throw new WorkInProgressException();
	}
	
	public void SetConfiguration(String filename) {
		configurationFileName=filename;
	}
	public void setMACM(MACM m) throws MissingOfferingException, MissingComponentsException {
		mymacm=m;
		myDeployment.setApp(mymacm);
//		myDeployment.readOfferings("src/main/resources/optimization/test1.properties");
//		Component[] comps=new Component[mymacm.getPeers().size()];
//		int i=0;
//		for (Entry<String, Peer> entry: mymacm.getPeers().entrySet()){
//			if (entry.getValue().getType()==macmPeerType.SaaS) {
//				comps[i]=new Component(""+i+","+entry.getValue().getName()+",1024,100");
//			}
//			i++;
//		}
//		myDeployment.setApp(comps);
	}

	public MACM getDeployable() {
		return myDeployment.getDeployable(mymacm);
	}
	
	public DeploymentInterface setDeployment(DeploymentInterface depl) {
		myDeployment=depl;
		return depl;
	}
	public DeploymentInterface getDeployment() {
		return myDeployment;
	}
	
//	public MACM getDeployable() {
//		MACM m=mymacm;
//		HashMap<VM,Integer> VMs=new HashMap<VM,Integer>();
//		HashMap<String,Integer> CSPs=new HashMap<String,Integer>();
//		if (myDeployment.status!=DeploymentMapBased.DeploymentStates.deployed) {
//			System.out.println("Deployment not evaluated");
//		} else {
//			for (Map.Entry<Component, VM> entry : myDeployment.deployment.entrySet()) {
//				if (!VMs.containsKey(entry.getValue())) {
//					VMs.put(entry.getValue(),1);
//					m.addIaaService(entry.getValue().VMtype);
//					if (!CSPs.containsKey(entry.getValue().CSP)) {
//						CSPs.put(entry.getValue().CSP, 1);
//						m.addCSP(entry.getValue().CSP);
//					}
//					m.addProvides(entry.getValue().CSP, entry.getValue().VMtype);
//				}
//				m.addHosts(entry.getValue().VMtype, entry.getKey().Name);
//				
//			}
//		}
//		return m;
//	}
	public abstract DeploymentInterface refreshDeployment();
	
	public void printDeployment() {
		myDeployment.print();
	}

	public void printDeploymentData() {
		myDeployment.printData();
		myDeployment.printDeploymentData();
	}
	
	public abstract void printOptimizationData();

	public static void main(String args[]) throws MissingOfferingException, MissingComponentsException {
		int i,N=1;//50;
		double[] costs=new double[N];
		long[] evaluations=new long[N];
		double cmean=0;
		int evmean=0;
		String c="",ev="";
		try {
			for(i=0;i<N;i++) {
				NeoDriver.cleanDB();
				NeoDriver.executeFromFile("src/main/resources/Models/TUTabstract");
				MACM macm= new MACM();
				macm.readNeo();
				Deployer deployer=new GeneratorOptimizer2();
//				Deployer deployer=new GeneticOptimizer2();
				deployer.setMACM(macm);
				deployer.setOfferings("src/main/resources/optimization/test1.properties");
				Scanner scan= new Scanner (System.in);
				System.out.println("The abstract application is in Graph DB,  press enter to deploy2");
				scan.nextLine();
				deployer.deploy();
				deployer.refreshDeployment();
				MACM m2=deployer.getDeployable();
				NeoDriver.cleanDB();
				m2.writeNeo();
				deployer.printDeployment();
				costs[i]=deployer.finalcost;
				c+=" "+String.format( "%1$,.2f",costs[i]);
				cmean+=costs[i];
				evaluations[i]=deployer.evaluated;
				ev+=" "+evaluations[i];
				evmean+=evaluations[i];
			}
			System.out.println("Mean Cost:"+String.format( "%1$,.2f",cmean/N)+" Costs:"+c);
			System.out.println("Mean Evaluations:"+(evmean/N)+" Evaluations:"+ev);
			System.exit(0);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


//	public String getOptimizationData() {
//		// TODO Auto-generated method stub
//		String data=""+evaluated+";"+validEvaluated+";"+finalcost+";";
//		return data;
//	}
	
	public void setConfiguration(Properties p) {
		configuration=p;
	}
	
	public abstract String getOptimizationData() ;
	public abstract String problemDescriptor(); 
	public abstract String getOptimizedSolution();
}
