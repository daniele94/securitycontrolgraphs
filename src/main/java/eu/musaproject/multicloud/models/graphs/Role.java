package eu.musaproject.multicloud.models.graphs;

public class Role {

    public String role;
    public Peer peer;
    public String protocol;


    public Role(String role, Peer peer, String  protocol)
    {
        this.role=role;
        this.peer=peer;
        this.protocol=protocol;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Peer getPeer() {
        return peer;
    }

    public void setPeer(Peer peer) {
        this.peer = peer;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }
}
