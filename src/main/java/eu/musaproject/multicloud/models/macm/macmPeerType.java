package eu.musaproject.multicloud.models.macm;

import eu.musaproject.multicloud.models.graphs.TypeInterface;
import eu.musaproject.multicloud.models.macmSLA.SLARelationshipType;

public enum macmPeerType implements TypeInterface {
	CSP("CSP"),
	CSC("CSC"),
	party("party"),
	IaaS("IaaS"),
	PaaS("PaaS"),
	SaaS("SaaS"),
	service("service"),
	SLA("SLA"),
	SLAT("SLAT");

	private final String itsName;

	macmPeerType(String name) {
		itsName=name;
	}

	public String getName() {
		// TODO Auto-generated method stub
		return this.itsName;
	}
	
	public TypeInterface isInType(String t) {
		// TODO Auto-generated method stub
		   for (macmPeerType c : macmPeerType.values()) {
		        if (c.name().equals(t)) {
		            return c;
		        }
		    }
		return null;
	}
}

