package eu.musaproject.multicloud.models.macm;

public enum RelationshipType {
    hosts,
    uses,
    provides,
    owns,
    supports,
    grants
}
