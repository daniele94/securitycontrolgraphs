package eu.musaproject.multicloud.models.macm;

import eu.musaproject.multicloud.models.graphs.TypeInterface;
import eu.musaproject.multicloud.models.macmSLA.SLARelationshipType;

public enum macmRelationshipType implements TypeInterface{
	hosts("hosts"),
	uses("uses"),
	provides("provides"),
	owns("owns"),
	requires("requires"),
	supports("supports"),
	grants("grants");

	private final String itsName;

	macmRelationshipType(String name) {
		itsName=name;
	}


	public String getName() {
		// TODO Auto-generated method stub
		return this.itsName;
	}


	public TypeInterface isInType(String t) {
		// TODO Auto-generated method stub
		   for (macmRelationshipType c : macmRelationshipType.values()) {
		        if (c.name().equals(t)) {
		            return c;
		        }
		    }
		return null;
	}
}
