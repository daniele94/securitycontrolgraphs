package eu.musaproject.multicloud.models.data.caiqCA;

public class CSP {
	  String Name;
	  int ProviderId;
	  String ImageUrl;
	  String Location;
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public int getProviderId() {
		return ProviderId;
	}
	public void setProviderId(int providerId) {
		ProviderId = providerId;
	}
	public String getImageUrl() {
		return ImageUrl;
	}
	public void setImageUrl(String imageUrl) {
		ImageUrl = imageUrl;
	}
	public String getLocation() {
		return Location;
	}
	public void setLocation(String location) {
		Location = location;
	}
	  
}

