package eu.musaproject.multicloud.models.macmSLA;

import eu.musaproject.multicloud.models.graphs.TypeInterface;

public enum SLARelationshipType implements TypeInterface{
	ControlFamilyIn("ControlFamilyIn"),
	SecurityControlOf("SecurityControlOf"),
	SLOin("SLOin"),
	MeasuredWith("MeasuredWith"),
	MappedTo("MappedTo");

	private final String itsName;

	SLARelationshipType(String name) {
		itsName=name;
	}

	public String getName() {
		// TODO Auto-generated method stub
		return this.itsName;
	}
	
	public TypeInterface isInType(String t) {
		// TODO Auto-generated method stub
		   for (SLARelationshipType c : SLARelationshipType.values()) {
		        if (c.name().equals(t)) {
		            return c;
		        }
		    }
		return null;
	}
}
