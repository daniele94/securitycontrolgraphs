package eu.musaproject.multicloud.models.macmSLA;

import eu.musaproject.multicloud.models.graphs.TypeInterface;

public enum SLAPeerType implements TypeInterface {
	ControlFramework("ControlFramework"),
	ControlFamily("ControlFamily"),
	SecurityControl("SecurityControl"),
	SecurityMetric("SecurityMetric"),
	SLO("SLO"),
	SLA("SLA"),
	SLAT("SLAT");

	private final String itsName;

	SLAPeerType(String name) {
		itsName=name;
	}

	public String getName() {
		// TODO Auto-generated method stub
		return this.itsName;
	}

	public TypeInterface isInType(String t) {
		// TODO Auto-generated method stub
		   for (SLAPeerType c : SLAPeerType.values()) {
		        if (c.name().equals(t)) {
		            return c;
		        }
		    }
		return null;
	}
}

