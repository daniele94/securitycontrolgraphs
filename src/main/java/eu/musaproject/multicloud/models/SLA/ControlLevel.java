package eu.musaproject.multicloud.models.SLA;

public enum ControlLevel {
	domain, 
	subdomain,
	control,
	enhancement
}
