package eu.musaproject.multicloud.models.SLA;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class NISTmap extends CFmap{
    public static final Map<Integer, ControlLevel> Controlevels;
    static {
        Map<Integer, ControlLevel> aMap = new HashMap<Integer, ControlLevel>();
        aMap.put(1, ControlLevel.domain);
        aMap.put(2, ControlLevel.subdomain);
        aMap.put(3, ControlLevel.control);
        aMap.put(4, ControlLevel.enhancement);
        Controlevels = Collections.unmodifiableMap(aMap);
    }
    
    public static final Map<ControlLevel, String> FrameworkNames;
    static {
        Map<ControlLevel, String> aMap = new HashMap<ControlLevel, String>();
        aMap.put(ControlLevel.domain,"Family");
        aMap.put(ControlLevel.subdomain,"control");
        aMap.put(ControlLevel.control,"control");
        aMap.put(ControlLevel.enhancement,"enhancement");

        FrameworkNames = Collections.unmodifiableMap(aMap);
    }
    
}