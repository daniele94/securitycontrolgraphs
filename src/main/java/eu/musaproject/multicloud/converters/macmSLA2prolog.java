package eu.musaproject.multicloud.converters;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Map;

import com.ugos.jiprolog.engine.JIPEngine;
import com.ugos.jiprolog.engine.JIPQuery;
import com.ugos.jiprolog.engine.JIPTerm;
import com.ugos.jiprolog.engine.JIPVariable;

import eu.musaproject.multicloud.models.graphs.*;
import eu.musaproject.multicloud.models.macmSLA.SLAPeerType;
import eu.musaproject.multicloud.models.macmSLA.macmSLA;

public class macmSLA2prolog {
    static JIPEngine jip;

    static public void init() {
    	jip = new JIPEngine();
    }
	private static PrintWriter p;

	static public ArrayList<String> control_rules(macmSLA sla) {
		Map<String,Peer> peers=sla.getPeers();
		ArrayList<String> rules =new ArrayList<String>();
		JIPTerm queryTerm = null;
		System.out.println("size:"+peers.size());
		for(Map.Entry<String, Peer> entry : peers.entrySet()) {
			System.out.println(entry.getValue().getName());
			if(entry.getValue().type==SLAPeerType.SecurityControl) {
				queryTerm = jip.getTermParser().parseTerm("sla("+entry.getValue().getName().toLowerCase()+",csp).");
				jip.asserta(queryTerm);
				rules.add("sla("+entry.getValue().getName().toLowerCase()+",csp).");
			}
		}
		return rules;		
	}
	
	static public void query(String query) {
		JIPTerm queryTerm=jip.getTermParser().parseTerm(query);
		
        // open Query
        JIPQuery jipQuery = jip.openSynchronousQuery(queryTerm);
        JIPTerm solution;

        // Loop while there is another solution
        while (jipQuery.hasMoreChoicePoints())
        {
            solution = jipQuery.nextSolution();
            if (solution!=null) {
	            System.out.println(solution);

	            JIPVariable[] vars = solution.getVariables();
	            for (JIPVariable var : vars) {
	                if (!var.isAnonymous()) {
	                    System.out.print(var.getName() + " = " + var.toString(jip) + " ");
	                    System.out.println();
	                }
	            }
            } else {
            	System.out.println("no solution");
            }
        }
    }

	
	static public void print(ArrayList<String >rules) {

		try {
			try {
				p = new PrintWriter("/Users/maxrak/Documents/wip/csp.pl", "UTF-8");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			macmSLA sla =new macmSLA();
			sla.readNeo("CSP_SLA");
			for (int i=0;i<rules.size();i++){
				System.out.println(rules.get(i));
				p.println(rules.get(i));
			}
			p.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	static public void main(String args[]) {
		init();
		macmSLA sla =new macmSLA();
		sla.readNeo("CSP_SLA");
		System.out.println("oo");
		ArrayList<String> rules=control_rules(sla);
		query("sla(X,csp)");	
		//print(rules);
	}
}
