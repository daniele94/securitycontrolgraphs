package eu.musaproject.multicloud.converters;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import com.google.gson.Gson;

import eu.musaproject.multicloud.models.data.caiqCA.caiqCA;
import eu.musaproject.multicloud.models.macmSLA.macmSLA;

public class caiqCA2wsag {

	public static caiqCA readFromFile(String file) {
        Gson gson = new Gson();
        caiqCA caiq = null;
        try (Reader reader = new FileReader(file)) {

			// Convert JSON to Java Object
           caiq = gson.fromJson(reader, caiqCA.class);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
		return caiq;
    }

	public static macmSLA createFromCAiq(caiqCA caiq) {
		macmSLA sla=new macmSLA();
		return sla;
	}
	
    public static void main(String[] args) {
    		caiqCA caiq= readFromFile("src/main/resources/SLAs/Aimes.ccm.json");
    		System.out.println("caiq for"+caiq.getCsp().getName());
    }
}
