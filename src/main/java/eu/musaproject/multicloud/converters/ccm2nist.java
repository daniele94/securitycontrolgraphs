package eu.musaproject.multicloud.converters;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.musaproject.multicloud.models.graphs.Peer;
import eu.musaproject.multicloud.models.macmSLA.SLAPeerType;
import eu.musaproject.multicloud.models.macmSLA.macmSLA;

public class ccm2nist {
	macmSLA ccm;
	macmSLA nist;
	Map<String,List<String>> map; 

	public ccm2nist(){
		ccm=null;
		nist=null;
		map=null;
	}

	public macmSLA SetCCM(macmSLA sla) {
		ccm=sla;
		nist= convert();
		return ccm;
	}

	public void setupMap() {
		map=new HashMap<String,List<String>>();
		Connection connection = null;  
		ResultSet resultSet = null;  
		Statement statement = null;  

		try 
		{  
			Class.forName("org.sqlite.JDBC");  
			connection = DriverManager.getConnection("jdbc:sqlite:src/main/resources/data/ccm2nist.db");  
			statement = connection.createStatement();  
			resultSet = statement  
					.executeQuery("SELECT field1 as CCM, field2 as NIST FROM CCM2NIST");
			resultSet.next();
			while (resultSet.next()) 
			{  
				List<String> nistcontrols=getNISTs(resultSet.getString("NIST"));
				map.put(resultSet.getString("CCM").replace('-', '_'), nistcontrols);
				//System.out.println("CCM:"+ resultSet.getString("CCM")+resultSet.getString("NIST"));
				
			}  
		} 
		catch (Exception e) 
		{  
			e.printStackTrace();  
		}
		finally 
		{  
			try 
			{  
				resultSet.close();  
				statement.close();  
				connection.close();  
			} 
			catch (Exception e) 
			{  
				e.printStackTrace();  
			}  
		} 
	}
	private List<String> getNISTs(String nist) {
		List<String> controls=new ArrayList<String>();
		nist.trim();
		String[] s=nist.split("NIST SP800-53 R3");
		for(int i=0;i<s.length;i++) {
			
			if((s[i].trim()).isEmpty())  {
			//	System.out.print("NIST["+i+"] empty");
			} else { 
				//System.out.print(" NIST["+i+"]:"+NISTname(s[i].trim()));
				controls.add(NISTname(s[i].trim()));
			}
		}
//		System.out.println("");
		return controls;
	}

	private String NISTname(String SC) {
		// TODO Auto-generated method stub
		SC=SC.replace('-', '_');
		SC=SC.replaceAll("\\s+","");
		SC=SC.replace('(', '_');
		SC=SC.replace(')', ' ');
		return SC;
	}

	public macmSLA convert() {
		nist=new macmSLA(ccm.getName());
		List<Peer> ccmp=ccm.getPeersByType(SLAPeerType.SecurityControl);
		for(int i=0;i<ccmp.size();i++) {
			String ccmc=ccmp.get(i).getName();
//			System.out.println(ccmc);
			List<String> nists=map.get(ccmc);
//			System.out.println("Get Controls for"+ccmc);
			for (int j=0;j<nists.size();j++) {
				nist.addSecurityControl(nists.get(j));
			}
		}
		return nist;
	}
	public macmSLA getNIST(macmSLA ccmsla) {
		SetCCM(ccmsla);
		return nist;
	}

	public macmSLA getNIST() {
		return nist;
	}
	static public void main(String args[]) {
		ccm2nist c=new ccm2nist();
		c.setupMap();
	}
}
