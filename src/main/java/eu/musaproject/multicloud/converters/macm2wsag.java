package eu.musaproject.multicloud.converters;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import eu.musaproject.multicloud.engines.composition.NISTrules;
import eu.musaproject.multicloud.models.graphs.Peer;
import eu.musaproject.multicloud.models.macm.MACM;
import eu.musaproject.multicloud.models.macmSLA.SLAPeerType;
import eu.musaproject.multicloud.models.macmSLA.macmSLA;
import eu.musaproject.slagenerator.agreementoffermodel.AbstractMetricType;
import eu.musaproject.slagenerator.agreementoffermodel.AbstractMetricType.AbstractMetricDefinition;
import eu.musaproject.slagenerator.agreementoffermodel.AbstractSecurityControl;
import eu.musaproject.slagenerator.agreementoffermodel.AgreementOffer;
import eu.musaproject.slagenerator.agreementoffermodel.CapabilityType;
import eu.musaproject.slagenerator.agreementoffermodel.Context;
import eu.musaproject.slagenerator.agreementoffermodel.ControlFramework;
import eu.musaproject.slagenerator.agreementoffermodel.CustomServiceLevel;
import eu.musaproject.slagenerator.agreementoffermodel.GuaranteeTerm;
import eu.musaproject.slagenerator.agreementoffermodel.NISTSecurityControl;
import eu.musaproject.slagenerator.agreementoffermodel.ObjectiveList;
import eu.musaproject.slagenerator.agreementoffermodel.OneOpOperator;
import eu.musaproject.slagenerator.agreementoffermodel.SLOType;
import eu.musaproject.slagenerator.agreementoffermodel.SLOexpressionType;
import eu.musaproject.slagenerator.agreementoffermodel.SLOexpressionType.OneOpExpression;
import eu.musaproject.slagenerator.agreementoffermodel.ServiceDescriptionTerm;
import eu.musaproject.slagenerator.agreementoffermodel.ServiceDescriptionType;
import eu.musaproject.slagenerator.agreementoffermodel.ServiceDescriptionType.Capabilities;
import eu.musaproject.slagenerator.agreementoffermodel.ServiceDescriptionType.SecurityMetrics;
import eu.musaproject.slagenerator.agreementoffermodel.ServiceLevelObjective;
import eu.musaproject.slagenerator.agreementoffermodel.Term;
import eu.musaproject.slagenerator.agreementoffermodel.Terms;
import eu.musaproject.slagenerator.agreementoffermodel.Terms.All;

public class macm2wsag {
	static NISTrules nist;



	public macm2wsag() throws SQLException {
		nist=new NISTrules();
		nist.UpdateControlsList();
	}

	public static AgreementOffer macm2wsagOffer(MACM macm,String provider) {
		nist=new NISTrules();
		try {
			nist.UpdateControlsList();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		AgreementOffer offer = new  AgreementOffer();
		macmSLA sla=new macmSLA( provider+"_sla", Integer.valueOf(macm.getMcAppid()));
		Map<String,NISTSecurityControl> controlsmap=nist.getControlsMap();
		sla.readNeo(provider+"_sla");
		//Set Agreement Name
		offer.setName(provider+"_sla");

		//Set Agreement Context
		Context c=new Context();
		c.setAgreementInitiator("user");
		c.setAgreementResponder(provider);
		c.setServiceProvider(provider);
		c.setTemplateName(provider+"_slat");
		offer.setContext(c);


		//Create Capability
		ControlFramework nistfmw=new ControlFramework();
		nistfmw.setFrameworkName("NIST Control framework 800-53 rev. 4");
		nistfmw.setId("NIST_800_53_r4");
		List<AbstractSecurityControl> scs=new ArrayList<AbstractSecurityControl>();

		List<Peer> controls=sla.getPeersByType(SLAPeerType.SecurityControl);
		for(int i=0;i<controls.size();i++) {
			//Get Control from MACM
			String scid=controls.get(i).getName();
			System.out.println("macm2wsag Adding "+ scid);
			//Create NIST xml representation
			//			String [] scdata=scid.split("_");			
			//			NISTSecurityControl sc=new NISTSecurityControl();
			//			sc.setControlFamily(scdata[0].toUpperCase());
			//			if(scdata.length>2) {
			//				sc.setControlEnhancement(scdata[2]);				
			//			}	
			//			sc.setId(scid);
			//Add to capability
			NISTSecurityControl sc=controlsmap.get(scid.toLowerCase());
			scs.add(sc);
		}
		nistfmw.setSecurityControl(scs);

		//Set Agreement Terms
		ServiceDescriptionTerm sdt=new ServiceDescriptionTerm();
		sdt.setName(provider);
		sdt.setServiceName(provider);
		ServiceDescriptionType serviceDescriptionType=new ServiceDescriptionType();
		Capabilities cs=new Capabilities();
		serviceDescriptionType.setCapabilities(cs);


		//Add Capability
		CapabilityType capability =new CapabilityType();
		capability.setControlFramework(nistfmw);
		serviceDescriptionType.getCapabilities().getCapability().add(capability);

		//Start of Metrics Manage
		SecurityMetrics securityMetrics = new SecurityMetrics();

		List<AbstractMetricType> abstractMetricTypes = new ArrayList<AbstractMetricType>();

		//TODO: CICLO su metriche
		AbstractMetricType abstractMetricType = new AbstractMetricType();
		abstractMetricType.setName(""/*componentControlMetric.getMetric().getMetricName()*/);
		abstractMetricType.setReferenceId(""/*componentControlMetric.getMetric().getMetricId()*/);

		AbstractMetricDefinition abstractMetricDefinition = new AbstractMetricDefinition();
		abstractMetricDefinition.setDefinition(""/*componentControlMetric.getMetric().getMetricDescription()*/);
		abstractMetricDefinition.setExpression(""/*componentControlMetric.getMetric().getFormula()*/);

		abstractMetricType.setAbstractMetricDefinition(abstractMetricDefinition);

		abstractMetricTypes.add(abstractMetricType);
		//Fine CICLO su metriche

		securityMetrics.setSecurityMetric(abstractMetricTypes);

		//Add metrics to Service Description Type
		serviceDescriptionType.setSecurityMetrics(securityMetrics);
		//End of Metrics Manage

		//NIST Terms pieces
		sdt.setServiceDescription(serviceDescriptionType);
		List<Term> termsList=new ArrayList<Term>();
		termsList.add(sdt);


		//Start of Guarantee Term
		GuaranteeTerm guaranteeTerm = new GuaranteeTerm();
		guaranteeTerm.setName(""/*component.getName()*/);

		ServiceLevelObjective serviceLevelObjective = new ServiceLevelObjective();
		CustomServiceLevel customServiceLevel = new CustomServiceLevel();

		ObjectiveList objectiveList = new ObjectiveList();

		List<SLOType> slos = new ArrayList<SLOType>();

		int sloCount = 0;
		
		//TODO: CICLO sugli SLO
		SLOType slo = new SLOType();

		slo.setSLOID("slo"+String.valueOf(sloCount));
		slo.setMetricREF(""/*componentControlMetric.getMetric().getMetricId()*/);

		SLOexpressionType sLOexpressionType = new SLOexpressionType();

		OneOpExpression oneOpExpression = new OneOpExpression();
		oneOpExpression.setOperand(""/*componentControlMetric.getValue()*/);
		oneOpExpression.setOperator(getOperatorFromString(""/*componentControlMetric.getMetric().getOperator()*/));

		sLOexpressionType.setOneOpExpression(oneOpExpression);

		slo.setSLOexpression(sLOexpressionType);

		slos.add(slo);
		sloCount++;
		//Fine ciclo sugli SLO

		objectiveList.setSlo(slos);

		customServiceLevel.setObjectiveList(objectiveList);
		serviceLevelObjective.setCustomServiceLevel(customServiceLevel);
		guaranteeTerm.setServiceLevelObjective(serviceLevelObjective);
		termsList.add(guaranteeTerm);
		//End of Guarantee Term


		Terms t=new Terms();
		All a=new All();	
		a.setAll(termsList);
		t.setAll(a);



		offer.setTerms(t);
		return offer;
	}

	private static OneOpOperator getOperatorFromString(String value){
		if(value.equals("ge (>=)") || value.equals("ge(>=)")){
			return OneOpOperator.GREATER_THAN_OR_EQUAL;
		}else if(value.equals("leq (<=)") || value.equals("leq(<=)")){
			return OneOpOperator.LESS_THAN_OR_EQUAL;
		}else if(value.equals("eq (=)") || value.equals("eq(=)")){
			return OneOpOperator.EQUAL;
		}else if(value.equals("le (<=)") || value.equals("le(<=)")){
			return OneOpOperator.LESS_THAN_OR_EQUAL;
		}

		return null;
	}

	public static String macm2wsagString(MACM macm,String provider) {
		AgreementOffer offer = macm2wsagOffer(macm,provider);		
		String wsag=null;
		try {
			JAXBContext jc = JAXBContext.newInstance(AgreementOffer.class);
			Marshaller marshaller = jc.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			StringWriter sw = new StringWriter();
			marshaller.marshal(offer,sw);
			wsag = sw.toString();
		} catch (JAXBException e) {
			System.out.println("JAXBException "+e.toString());
		}
		return wsag;
	}

	public static void macm2wsagFile(MACM macm,String provider,String file) throws FileNotFoundException, UnsupportedEncodingException {
		AgreementOffer offer = macm2wsagOffer(macm,provider);		
		String wsag=null;
		try {
			PrintWriter writer = new PrintWriter(file, "UTF-8");
			JAXBContext jc = JAXBContext.newInstance(AgreementOffer.class);
			Marshaller marshaller = jc.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			//StringWriter sw = new StringWriter();
			marshaller.marshal(offer,writer);
			//wsag = sw.toString();
		} catch (JAXBException e) {
			System.out.println("JAXBException "+e.toString());
		}
	}

	public static void main(String args[]) {
		MACM macm=new MACM();
		macm.readNeo();
		macm.print();
		String a=macm2wsagString(macm,"db");
		System.out.println(a);
	}
}
