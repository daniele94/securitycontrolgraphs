package eu.musaproject.multicloud.converters;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import eu.musaproject.multicloud.models.macm.MACM;
import eu.musaproject.multicloud.models.macmSLA.macmSLA;

enum states {
	unsetted,
	created,
	xmlavailable,
	ready
}
public class caiqTree2macmSLA {
	states state;
	macmSLA sla;
	Document caiq;

	public caiqTree2macmSLA() {
		sla=new macmSLA();
		caiq=null;
		state=states.unsetted;
	}

	public caiqTree2macmSLA(String filename) {
		state=states.created;
		DocumentBuilder dBuilder;
		try {
			dBuilder = DocumentBuilderFactory.newInstance()
					.newDocumentBuilder();
			caiq = dBuilder.parse(filename);
			sla=new macmSLA();			
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public macmSLA readsla() throws convertException {

		if(state==states.unsetted) {
			throw (new convertException("Unsetted caiq file"));
		} 

		Node root=getCAIQroot();
		sla.SetName(root.getAttributes().getNamedItem("Id").getNodeValue()+"_sla");
		NodeList families=root.getChildNodes();
		for (int i=1;i<families.getLength();i++) {
			Node family=families.item(i);
			NamedNodeMap attrs=family.getAttributes();
			if(attrs!=null) {
				Node attr=attrs.getNamedItem("Id");
				if(attr!=null) {
					String CF=attr.getNodeValue();
					sla.addControlFamily(CF);
					getControls(families.item(i));
				}
			}
			
		}
		return sla;
	}

	public Node getCAIQroot(){
		NodeList nodes=caiq.getChildNodes().item(0).getChildNodes();
		Node root=nodes.item(1);
		System.out.println("CAIQ: "+getId(root));		
		return root;
	}

	private String getId(Node n) {
		String id=null;
		NamedNodeMap attrs=n.getAttributes();
		if(attrs!=null) {
			Node attrNode=attrs.getNamedItem("Id");	
			if(attrNode!=null)	{
				id=attrNode.getNodeValue();
			}
		}
		return id;
	}
	
	private void getControls(Node familyNode) {
		NodeList controls=familyNode.getChildNodes();
		int length=controls.getLength();
		String control;
		for (int i=1;i<length;i++) {
			Node next=controls.item(i);
			control=getId(next);
			if(control!=null) {
				control=control.replace('-', '_');
//				System.out.print("control:"+control);
				if (IsControlValid(next)) {
					sla.addSecurityControl(control);
//					System.out.println(" valid");
				} else {
//					System.out.println(" not valid");
				}
			}
		}		
	}
	
	private boolean IsControlValid(Node control) {
		boolean result=true;
		NodeList questions=control.getChildNodes();
		List<String> answers=getAnswers(questions);
		for (int i=0;i<answers.size();i++) {
			result=result&&answers.get(i).equals("YES");
		}
		return result;
	}
	

	private List<String> getAnswers(NodeList questions) {
		List<String> answers=new ArrayList<String>();
		for (int i=1;i<questions.getLength();i++) {
			Node q=questions.item(i);
			String id=getId(q);
			if(id!=null) {
//				System.out.print(" question:"+id);
				String answer=getAnswer(q);
				answers.add(answer);
//				System.out.print(" "+answer);
			}
		}
		return answers;
	}
	
	private String getAnswer(Node question) {
		String answer="err";
		NodeList Values=question.getChildNodes();
		for (int i=0;i<Values.getLength();i++) {
			Node value=Values.item(i);
			if (value.getNodeName().equals("Value")) {
				answer=value.getTextContent();
			}
		}
		return answer;
	}

	static String readFile(String path, Charset encoding) 
			throws IOException 
	{
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}



	public void ReadCaiq(String filename) throws IOException, ParserConfigurationException, SAXException{
		String caiq=readFile(filename,StandardCharsets.UTF_8);
		DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance()
				.newDocumentBuilder();

		Document doc = dBuilder.parse(filename);
		NodeList nodes=doc.getChildNodes().item(0).getChildNodes();
		Node root=nodes.item(1);
		System.out.print("Root element: " + root.getNodeName()+" "+nodes.getLength());
		NamedNodeMap rootAtts=root.getAttributes();
		System.out.println(rootAtts.item(0));

	}

	public static void main(String args[]) throws ParserConfigurationException, SAXException {
		try {
			String file="src/main/resources/SLAs/AmazonEC2-CAIQ-v2.xml";
			caiqTree2macmSLA c2w=new caiqTree2macmSLA(file);
			macmSLA sla=c2w.readsla();
			//sla.print();
			ccm2nist c=new ccm2nist();
			c.setupMap();
			macmSLA nistSLA=c.getNIST(sla);
			nistSLA.writeNeo();			
//			String a=macm2wsagString(macm,"AmazonEC2");
//			System.out.println(a);
		} catch (convertException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}
}
