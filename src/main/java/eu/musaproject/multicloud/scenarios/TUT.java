package eu.musaproject.multicloud.scenarios;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

import eu.musaproject.multicloud.converters.wsag2neo;
import eu.musaproject.multicloud.engines.composition.CS3Mcomposer;
import eu.musaproject.multicloud.engines.composition.NISTrules;
import eu.musaproject.multicloud.engines.composition.SLAcomposer;
import eu.musaproject.multicloud.engines.composition.SLAcomposerFile;
import eu.musaproject.multicloud.models.graphs.Relationship;
import eu.musaproject.multicloud.models.macmSLA.macmSLA;
import eu.musaproject.multicloud.models.macmSLA.macmSLAT;

public class TUT extends SLAcomposer {

	public TUT() throws SQLException {
		super();
		// TODO Auto-generated constructor stub
	}

	static String readFile(String path, Charset encoding) 
			throws IOException 
	{
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}

//	public static void main(String[] args) {
//		SLAcomposerFile slac;
//		try {
//			slac = new SLAcomposerFile();
//			Scanner scan=new Scanner(System.in);
//			slac.setAppFromFile("src/main/resources/MUSA/TUT/MACM_TUT_with_CSP.macm");
//			slac.addSLAfromWSAG("Aimes_sla","Aimes", "src/main/resources/MUSA/TUT/CSP_SLAs/Aimes.xml");
//
//			slac.addSLATfromWSAG("JourneyPlannerVM_slat", "JourneyPlannerVM", "src/main/resources/MUSA/TUT/VM_SLATs/VM_2core_4096RAM_AIMES.xml");
//			slac.addSLATfromWSAG("TSMEngineHostVM_slat", "TSMEngineHostVM", "src/main/resources/MUSA/TUT/VM_SLATs/VM_2core_4096RAM_AIMES.xml");
//			slac.addSLATfromWSAG("ConsumptionEstimatorVM_slat", "ConsumptionEstimatorVM", "src/main/resources/MUSA/TUT/VM_SLATs/VM_2core_4096RAM_AIMES.xml");
//			slac.addSLATfromWSAG("JourneyDatabaseVM_slat", "JourneyDatabaseVM", "src/main/resources/MUSA/TUT/VM_SLATs/VM_2core_4096RAM_AIMES.xml");
//
//			slac.addSLATfromWSAG("JourneyPlanner_slat", "JourneyPlanner", "src/main/resources/MUSA/TUT/SAAS_SLATs/JourneyPlanner.xml");
//			slac.addSLATfromWSAG("TSMEngineHost_slat", "TSMEngine", "src/main/resources/MUSA/TUT/SAAS_SLATs/TSMEngine.xml");
//			slac.addSLATfromWSAG("ConsumptionEstimator_slat", "ConsumptionEstimator", "src/main/resources/MUSA/TUT/SAAS_SLATs/ConsumptionEstimator.xml");
//			slac.addSLATfromWSAG("JourneyDatabase_slat", "JourneyDatabase", "src/main/resources/MUSA/TUT/SAAS_SLATs/JourneyDatabase.xml");
//
//			slac.addSLATfromWSAG("ACAgentForTSMEngine_slat", "ACAgentForTSMEngine", "src/main/resources/MUSA/TUT/SAAS_SLATs/ACAgentForTSMEngine.xml");
//
//			slac.prepare();
//			String query="";
//
//			//Prolog Console
//			//			System.out.println("Go test");
//			//			while (!query.equals("stop")) {
//			//				System.out.println("Query");System.out.flush();
////							query=scan.nextLine();				
//			//				slac.slar.query(query);
//			//			} 
//			System.out.println("Press Enter to Compose");
//			query=scan.nextLine();				
//			slac.compose();
//			System.out.println("Press Enter to retrieve sla");
//			query=scan.nextLine();				
//
//			//			slac.compose("vm", "vm_sla");
//			//			slac.compose("db", "db_sla");
//			//			slac.compose("webapp", "webapp_sla");
//			macmSLA wsla=slac.getSLAfromNeo("webapp_sla");
//			wsla.print();
//		} catch (SQLException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}

	@Override
	public void setMACMinNeo() {
		// TODO Auto-generated method stub
		try {
			neo.executeFromFile("src/main/resources/MUSA/TUT/MACM_TUT_with_CSP.macm");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}

	@Override
	public void setSLATinNeo(String provider) {
		// TODO Auto-generated method stub
		String slatname=provider+"_slat";
		String wsag;
		try {
			wsag = readFile("src/main/resources/MUSA/TUT/SLAs3/"+slatname+".xml"
					,StandardCharsets.UTF_8);
			wsag2neo.wsagGraph(wsag,"SLAT",slatname,mcapp.getAppid());
			mcapp.addSLAT(slatname);
			Relationship r=mcapp.addSupports(provider, slatname);
			mcapp.syncNeoRelationship(r);
			macmSLAT slat=new macmSLAT();
			slat.readNeo(slatname);
			slar.addSLATtoKB(slat);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void setup() throws IOException {
		int i;
		neo.cleanDB();
		setMACMinNeo();
		mcapp.readNeo();
		
		List<String> services=Services();
		List<String> csps=CSPs();
		
		//Add the SLAs for the CSPs
		for (i=0;i<csps.size();i++){
			setSLAinNeo(csps.get(i));
		}
		
		//Add the SLATs for the services
		for (i=0;i<services.size();i++){
			setSLATinNeo(services.get(i));
		}
	}
	
	public void complete() throws FileNotFoundException, SQLException {
		createslas();
		NISTrules nist=new NISTrules();
//		nist.tablePrint(slas);
	}
	
	@Override
	public void setSLAinNeo(String provider) {
		String slaname=provider+"_sla";
		String wsag;
		try {
			wsag = readFile("src/main/resources/MUSA/TUT/SLAs3/"+slaname+".xml",StandardCharsets.UTF_8);
			wsag2neo.wsagGraph(wsag,"SLA",slaname,mcapp.getAppid());
			mcapp.addSLA(slaname);
			Relationship r=mcapp.addGrants(provider, slaname);
			mcapp.syncNeoRelationship(r);
			macmSLA sla=new macmSLA();
			sla.readNeo(slaname);
			slar.addSLAtoKB(sla);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public macmSLA getSLAfromNeo(String provider) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public macmSLAT getSLATfromNeo(String provider) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	public static void main(String args[]){
		try {
			TUT comp=new TUT();
			comp.setup();
			comp.mcapp.print();
			System.out.println("Press return to Prepare Compose");
			Scanner scan = new Scanner(System.in);
			String query = scan.nextLine();
			comp.prepare();
			System.out.println("Press return to Compose");
			scan = new Scanner(System.in);
			query = scan.nextLine();
			comp.compose();
//			comp.session();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
}
