package eu.musaproject.multicloud.scenarios;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Scanner;

import eu.musaproject.multicloud.converters.NIST_db2Neo;
import eu.musaproject.multicloud.converters.csv2neo;
import eu.musaproject.multicloud.converters.wsag2neo;
import eu.musaproject.multicloud.engines.composition.SLAreasoner;
import eu.musaproject.multicloud.engines.deployments.Deployer;
import eu.musaproject.multicloud.engines.deployments.MissingComponentsException;
import eu.musaproject.multicloud.engines.deployments.MissingOfferingException;
import eu.musaproject.multicloud.engines.deployments.optimization.GeneratorOptimizer2;
import eu.musaproject.multicloud.models.SLA.SLAgraphs;
import eu.musaproject.multicloud.models.graphs.Relationship;
import eu.musaproject.multicloud.models.macm.MACM;
import eu.musaproject.multicloud.models.macmSLA.macmSLA;
import eu.musaproject.multicloud.utilities.NeoDriver;

public class InfSce {

	public static void main(String args[]) throws IOException, MissingOfferingException, MissingComponentsException{
		NeoDriver.cleanDB();
		NeoDriver.executeFromFile("src/main/resources/InfSce/webapp.cyber");
		MACM macm= new MACM();
		macm.readNeo();
		Deployer deployer=new GeneratorOptimizer2();
		deployer.setMACM(macm);
		deployer.setOfferings("src/main/resources/InfSce/offering.properties");
		deployer.deploy();	
		deployer.printDeployment();
		MACM m2=deployer.getDeployable();
		NeoDriver.cleanDB();
		m2.writeNeo();
		deployer.printDeployment();
	}
}
