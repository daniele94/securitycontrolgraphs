package eu.musaproject.multicloud.scenarios;

import java.io.IOException;

import eu.musaproject.multicloud.converters.NIST_db2Neo;
import eu.musaproject.multicloud.converters.csv2neo;
import eu.musaproject.multicloud.converters.wsag2neo;
import eu.musaproject.multicloud.models.SLA.SLAgraphs;
import eu.musaproject.multicloud.utilities.NeoDriver;

public class WebPoolScenario {

	public static void main(String args[]) {

		try {
			NeoDriver.cleanDB();
			NIST_db2Neo.createNISTgraph();
			NeoDriver.executeFromFile("src/main/resources/Models/WebPoolScenario1/");

			wsag2neo.wsagReader("src/main/resources/SLAs/wsag.xml");
			SLAgraphs.addSLA("WebPool", "WebPool_SVA");
			
			wsag2neo.wsagReader("src/main/resources/SLAs/wsag2.xml");
			SLAgraphs.addSLA("myvm", "WebPool_SVA2");

			SLAgraphs.composeSLA("WebPool_SVA", "WebPool_SVA2", "csla");
//			SLAgraphs.SLAs();
//			SLAgraphs.PrintFamilies("csla");
			//SLAgraphs.ControlsWithoutMetric("csla");

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
