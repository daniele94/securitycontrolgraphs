package eu.musaproject.multicloud.scenarios;

import eu.musaproject.multicloud.converters.macm2wsag;
import eu.musaproject.multicloud.converters.wsag2neo;
import eu.musaproject.multicloud.models.macm.MACM;

public class ReadAndWriteTest {

	public static void main(String[] args) {

		wsag2neo.wsagReader("src/main/resources/MUSA/LHS/review/SLAs/Kafka_slat.xml");
		MACM macm = new MACM();
		macm.setAppid(535);
		String xml = macm2wsag.macm2wsagString(macm, "Kafka");
		System.out.println("XML: "+xml);
	}

}
