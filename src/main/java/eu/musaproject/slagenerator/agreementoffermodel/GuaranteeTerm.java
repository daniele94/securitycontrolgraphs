package eu.musaproject.slagenerator.agreementoffermodel;




import javax.xml.bind.annotation.*;
import java.io.Serializable;

/**
 * Created by adispataru on 4/24/15.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "serviceScope", "qualifyingCondition",
        "name", "obligated", "serviceLevelObjective", "businessValueList"
})
@XmlRootElement(name = "GuaranteeTerm", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
public class GuaranteeTerm extends Term implements Serializable{

    private static final long serialVersionUID = -596304085844960408L;
    @XmlAttribute(name = "Name", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
    private String name;
    @XmlAttribute(name = "Obligated", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
    private String obligated;
    @XmlElement(name = "ServiceScope", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
    private ServiceScope serviceScope;
    @XmlElement(name = "QualifyingCondition", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
    private boolean qualifyingCondition;
    @XmlElement(name = "ServiceLevelObjective", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
    private ServiceLevelObjective serviceLevelObjective;
    @XmlElement(name = "BusinessValueList", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
    private String businessValueList;


    public boolean isQualifyingCondition() {
        return qualifyingCondition;
    }

    public void setQualifyingCondition(boolean qualifyingCondition) {
        this.qualifyingCondition = qualifyingCondition;
    }

    public ServiceScope getServiceScope() {
        return serviceScope;
    }

    public void setServiceScope(ServiceScope serviceScope) {
        this.serviceScope = serviceScope;
    }

    public ServiceLevelObjective getServiceLevelObjective() {
        return serviceLevelObjective;
    }

    public void setServiceLevelObjective(ServiceLevelObjective serviceLevelObjective) {
        this.serviceLevelObjective = serviceLevelObjective;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getObligated() {
        return obligated;
    }

    public void setObligated(String obligated) {
        this.obligated = obligated;
    }

    public String getBusinessValueList() {
        return businessValueList;
    }

    public void setBusinessValueList(String businessValueList) {
        this.businessValueList = businessValueList;
    }


    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "serviceName",
    })
    @XmlRootElement(name = "ServiceScope", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
    public static class ServiceScope {
        @XmlAttribute(name = "ServiceName", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
        private String serviceName;

        public String getServiceName() {
            return serviceName;
        }

        public void setServiceName(String serviceName) {
            this.serviceName = serviceName;
        }
    }



}
