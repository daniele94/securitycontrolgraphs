package eu.musaproject.slagenerator.agreementoffermodel;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * Created by adrian on 11.01.2016.
 */
@XmlSeeAlso({CCMSecurityControl.class, NISTSecurityControl.class})
public class AbstractSecurityControl {

//    @XmlElements({
//            @XmlElement(name="id", type=NISTSecurityControl.class, namespace = "http://www.specs-project.eu/resources/schemas/xml/control_frameworks/nist"),
//            @XmlElement(name="id", type=CCMSecurityControl.class, namespace = "http://www.specs-project.eu/resources/schemas/xml/control_frameworks/ccm")
//    })
//    protected String id;

}
